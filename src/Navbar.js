import React from 'react'
import './Navbar.css';

import {NavLink} from 'react-router-dom'

function Navbar() {

    return <div className='Nav'>
        
        
        <ul>
            <NavLink to = '/services'>
            <li>
                Services
            </li>
            </NavLink>
            <NavLink to ='/contactus'>
            <li>
                Contact Us
            </li>
            </NavLink>
            <NavLink to ='/home' >
            <li>
                Home
            </li>
            </NavLink>
        </ul>


    </div>
}

export default Navbar