import React from 'react'
import Navbar from './Navbar'

function Home() {

    return <>
        <Navbar/>
    
    <div className='home'>
       <img className='img1' src='https://images.unsplash.com/photo-1454165804606-c3d57bc86b40?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxzZWFyY2h8NXx8aG9tZXBhZ2V8ZW58MHx8MHx8&auto=format&fit=crop&w=500&q=60'></img>
    </div></>
}

export default Home