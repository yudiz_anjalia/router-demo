import React from 'react'
import Home from './Home';

function Login(props) {
    const onSubmit = () => {
      localStorage.setItem('token', 'randomValue');
      props.history.push('/home')
    }
  
    return (
      <div className='box'>
        <form className='login'>
        <h2>Welcome to the Login Page!!</h2>
        <div>
          <div>Email</div>
          <div><input type="email" /></div>
        </div>
        <div>
          <div>Password</div>
          <div><input type="password" /></div>
        </div>
        <div className='button'>
         
          <button onClick={onSubmit} >Submit</button>
        </div>
      </form>
      </div>
    );
  }
  
  export default Login;