import './App.css';

import Home from './Home';
import Contactus from './Contactus';
import Services from './Services';
import Login from './Login';

import {BrowserRouter as Router , Switch, Route} from 'react-router-dom'
 
const PrivateRoute = (props) => {
  const token = localStorage.getItem('token');
  if (token) {
      return <Route exact={true} path={props.path} component={props.component} />
  } else {
      return <Login {...props} />
  }
}

function App() {
  return (
    <div className="App">
     
      <Router>
      
     
      <div>
        
      <Switch>
      
        <Route path = '/' component = {Login} exact/>
        <PrivateRoute path = '/home' component = {Home} exact/>
        <PrivateRoute path = '/contactus' component = {Contactus} exact />
        <PrivateRoute path = '/services' component = {Services} exact/> 

      </Switch>
      </div>
      </Router>
      
    </div>
  );
}

export default App;
